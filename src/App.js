import "./App.css";
import AppNavBar from "./components/AppNavBar";
/* The `BrowserRouter` component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser.
The `Routes` component holds all our Route components. It selects which `Route` component to show based on the URL Endpoint.
*/
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { UserProvider } from "./userContext";
// import Banner from "./components/Banner";
import { Container } from "react-bootstrap";
// import Highlights from "./components/Highlights";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import LoginUser from "./pages/LoginUser";
import Home from "./pages/Home";
import Logout from "./pages/Logout";
import ErrorPage from "./pages/ErrorPage";
import { useState, useEffect } from "react";
import CourseView from "./pages/CourseView";

function App() {
  // State hook for the user to store the information for a global scope
  // Initialized as an object with properties from the localStorage
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  // Function for clearing the localStorage when a user logout
  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  });
  return (
    // Fragments are needed when there are two or more compenents, pages, or html elements present in the code
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        {/* Self Closing Tags */}
        <AppNavBar />
        <Container>
          {/* <Home /> */}
          {/* <Banner />
        <Highlights /> */}
          {/* <Courses /> */}
          {/* <Register /> */}
          {/* <LoginUser /> */}
          <Routes>
            {/* path is the endpoint and element is the page */}
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<LoginUser />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/courseView/:courseId" element={<CourseView />} />
            <Route path="*" element={<ErrorPage />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
