// import { useState } from "react";
import { Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

/*
courseProp = { 
    description : "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor. Consectetur occaecat laborum exercitation sint reprehenderit irure nulla mollit. Do dolore sint deserunt quis ut sunt ad nulla est consectetur culpa. Est esse dolore nisi consequat nostrud id nostrud sint sint deserunt dolore."
    id : "wdc001"
    name : "PHP - Laravel"
    onOffer : true
    price : 45000
}
*/
export default function CourseCard({ courseProp }) {
  //Activity 50
  // console.log(props);
  // console.log(typeof props);

  const { name, description, price, _id } = courseProp;

  // getter -> stores the value. variable
  // setter -> it tells the computer how to store the value, it sets the value to be stored in the getter
  //(0) initial Getter Value
  //getter, setter
  // const [count, setCount] = useState(0);
  // const [seat, setSeat] = useState(30);

  // function enroll() {
  //   setCount(count + 1);
  //   setSeat(seat - 1);
  //   console.log("Enrollees: " + count);
  //   console.log("Seats: " + seat);
  // }
  // if (seat === 0) alert("No More Seats Available!");
  return (
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        {/* <Card.Subtitle>Enrollees:</Card.Subtitle>
        <Card.Text>{count} students</Card.Text>
        <Card.Subtitle>Seats:</Card.Subtitle>
        <Card.Text>{seats}</Card.Text> */}
        <Button variant="primary" as={Link} to={`/courseView/${_id}`}>
          Details
        </Button>
      </Card.Body>
    </Card>
  );
}

//Discussion
// {/*<Row className="mt-3 mb-3">
//       {/* <Col xs={12} md={4} > */}
//       <Col xs={12} md={4}>
//       <Card className="p-3">
//         <Card.Body>
//           <Card.Title>
//             <h5>Sample Course</h5>
//           </Card.Title>
//           <Card.Text>
//             <h6 className=" mb-0">Description</h6>
//             <p>This is a sample course offering.</p>
//           </Card.Text>
//           <Card.Text>
//             <h6 className="mb-0">Price</h6>
//             <p>Php 40,000</p>
//           </Card.Text>
//           <Button variant="primary">Enroll</Button>
//         </Card.Body>
//       </Card>
//       </Col>
//     </Row>*/}
