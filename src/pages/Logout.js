import { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../userContext";

export default function Logout() {
  //commented localstorage.clear since we used it in app.js unsetUser function
  // localStorage.clear();

  const { unsetUser, setUser } = useContext(UserContext);

  unsetUser();

  useEffect(() => {
    setUser({
      id: null,
    });
  });

  return <Navigate to="/login" />;
}
