import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../userContext";
import Swal from "sweetalert2";

export default function Register() {
  //for User Context
  const { user } = useContext(UserContext);
  // State hooks to store the values of our input fields
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState(" ");
  const [number, setNumber] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  // State to determine whether the submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  console.log(firstName);
  console.log(lastName);
  console.log(number);
  console.log(email); // email = h; value = h
  // onChange - checks if there are any changes inside of the input fields.
  // value = {email} the value stored in the input field will come from the value inside the getter "email"
  // setEmail(e.target.value) - sets the value of the getter email to the value stored in the value={email} inside the input field
  console.log(password1);
  console.log(password2);
  // useEffect() - whenever ther is a change in our webpage

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      number.length === 11 &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  });

  // function duplicateEmail (e) {
  //   e.preventDefault()
  //   fetch("http://localhost:4000/users/login", {
  //     method: "POST",
  //     headers: {
  //       "Content-Type": "application/json",
  //     }
  //     body: JSON.stringify({
  //       email: email,
  //       password
  //     })
  //   })

  // }

  function registerUser(e) {
    e.preventDefault();

    fetch("http://localhost:4000/users/checkEmail", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: "Email Duplicate",
            icon: "error",
            test: "Please use another email",
          });
        } else {
          fetch("http://localhost:4000/users/register", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              mobileNo: number,
              password: password1,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);

              if (data === true) {
                //clear input fields
                setFirstName("");
                setLastName("");
                setEmail("");
                setNumber("");
                setPassword1("");
                setPassword2("");

                Swal.fire({
                  title: "Registered Successfully",
                  icon: "success",
                  test: "Thank you for registering",
                });

                Navigate("/courses");
              } else {
                Swal.fire({
                  title: "Something wrong",
                  icon: "error",
                  text: "Please try again",
                });
              }
            });
        }
      });
  }

  // alert("Thank you for registering");

  return (
    // return user.email !== null ? (
    //   <Navigate to="/courses" />
    // ) : (
    <Form onSubmit={(e) => registerUser(e)}>
      <Form.Group className="mb-3" controlId="formBasicFirstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="First Name"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicLastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Last Name"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicNumber">
        <Form.Label>Contact Number</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter number"
          value={number}
          onChange={(e) => setNumber(e.target.value)}
          minLength={11}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
        />
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicVerifyPassword">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Verify Password"
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
        />
      </Form.Group>
      {/* Ternary Operator
        
        if (isActive === true) {
            <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
        } else {
            <Button variant="danger" type="submit" id="submitBtn">
          Submit
        </Button>
        }

      */}
      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="danger" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}
